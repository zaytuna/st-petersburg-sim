package visualization;

/* COMMENTS ARE GOOD
 * cases:
 * 	A) query nubmer of times to reach $x in profit overall.
 *	B) Query overall profet in x tries
 *	C) Find overall investment in making back the money
 *
 * In essence:
 *	Run until makes $moneys OR until x tries are up; display
 *	moneys made, moneys invested, and number of tries.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SimulationFrame extends JFrame implements Runnable, ActionListener, ChangeListener {
	private static final long serialVersionUID = -8794316142433949556L;

	int modUpdate = 1; // low means program runs slower; displays more
						// intermediate results.
	int delay = 0; // more visible results- purposefullly slows down the program

	long playerMoneys = 0;
	long minMoneys = 0;
	long numTries = 0;
	long time = 0;
	long maxMoneys = 0;

	int cost = 4;// in dollars, please...
	boolean stopNow = false;

	// Components
	JButton start = new JButton("Start");
	JLabel stop = new JLabel("Stop: ", JLabel.CENTER);
	JComboBox<String> stopWhen = new JComboBox<String>(new String[] { "When I tell you to", "When I've made back my money",
			"When I've made:", "When I've lost:", "When I've used: " });
	JSlider speed = new JSlider(JSlider.HORIZONTAL, 0, 1000, 0);
	JSlider modSpeed = new JSlider(JSlider.VERTICAL, 1, 100000, 1);
	JTextArea results = new JTextArea();
	JLabel end = new JLabel("");
	JTextField box = new JTextField();
	JTextField costField = new JTextField();
	int boxValue;// for performance reasons...

	String[] ends = { "", "", "dollars", "dollars", "tries" };
	boolean[] showBoxAndEnd = { false, false, true, true, true };

	public SimulationFrame() {
		setLayout(null);

		centerFrame(600, 410, this);
		start.setBounds(50, 340, 500, 30);
		stop.setBounds(60, 300, 50, 25);
		stopWhen.setBounds(110, 300, 203, 25);
		box.setBounds(320, 300, 44, 25);
		end.setBounds(370, 300, 60, 25);
		results.setBounds(250, 10, 320, 250);
		costField.setBounds(10, 10, 100, 25);
		speed.setBounds(10, 275, 400, 25);
		modSpeed.setBounds(10, 50, 25, 200);

		costField.setText("4");
		box.setText("10");

		results.setEditable(false);
		results.setBorder(new javax.swing.border.LineBorder(Color.BLACK, 4));

		getContentPane().setBackground(new Color(250, 250, 200));
		speed.setOpaque(false);
		modSpeed.setOpaque(false);

		add(box);
		add(end);
		add(stop);
		add(stopWhen);
		add(modSpeed);
		add(costField);
		add(results);
		add(speed);
		add(start);

		start.addActionListener(this);
		stopWhen.addActionListener(this);
		speed.addChangeListener(this);
		modSpeed.addChangeListener(this);

		box.setVisible(false);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == start && start.getText().equals("Start")) {
			stopNow = false;
			new Thread(this).start();
			start.setText("Stop");
		} else if (evt.getSource() == start && start.getText().equals("Stop")) {
			stop();
		} else if (evt.getSource() == stopWhen) {
			int index = stopWhen.getSelectedIndex();
			box.setVisible(showBoxAndEnd[index]);
			end.setVisible(showBoxAndEnd[index]);
			end.setText(ends[index]);
		}
	}

	public void stateChanged(ChangeEvent evt) {
		delay = speed.getValue();
		modUpdate = modSpeed.getValue();
	}

	public void run() {
		int count = 0;
		cost = Integer.parseInt(costField.getText());
		boxValue = Integer.parseInt(box.getText());

		while (!(((stopWhen.getSelectedIndex() == 4) && numTries > boxValue)
				|| ((stopWhen.getSelectedIndex() == 1) && playerMoneys >= 0 && numTries > 0)
				|| ((stopWhen.getSelectedIndex() == 2) && playerMoneys >= boxValue) || ((stopWhen.getSelectedIndex() == 3) && playerMoneys <= -boxValue))) {
			if (++count % modUpdate == 0)
				results.setText("Try " + numTries + "\nTotal profit so far: " + playerMoneys);
			playerMoneys -= cost;// player pays.

			boolean coin = true;
			int tableMoneys = 1;

			if (stopNow)
				break;

			while (coin) {
				time++;
				tableMoneys *= 2;
				coin = (int) (Math.random() * 2) == 0;
				if (count % modUpdate == 0)
					results.setText(results.getText() + "\nCoin is " + (coin ? "Heads" : "Tails"));
			}
			if (count % modUpdate == 0)
				results.setText(results.getText() + "\n\nAmount earned: " + (tableMoneys - cost) + "\nMax Earned: "
						+ maxMoneys + " (or 2^" + Math.round(Math.log(maxMoneys) / Math.log(2)) + ")");
			playerMoneys += tableMoneys;
			if (minMoneys > playerMoneys)
				minMoneys = playerMoneys;
			if (maxMoneys < tableMoneys)
				maxMoneys = tableMoneys;
			numTries++;

			try {
				Thread.sleep(delay);
			} catch (Exception e) {
			}

		}

		results.setText("Total made: " + playerMoneys + "\nTotal investment: " + (-minMoneys) + "\nTimes Played: "
				+ numTries + "\nTime required to play: " + ((time / 60) / 24) + " days, " + ((time / 60) % 24)
				+ " hours, and " + (time % 60) + " minutes." + "\nLargest amout earned: " + maxMoneys + " (or 2^"
				+ Math.round(Math.log(maxMoneys) / Math.log(2)) + ")");
		numTries = 0;
		playerMoneys = 0;
		minMoneys = 0;
		maxMoneys = 0;
		time = 0;
		start.setText("Start");
	}

	public void stop() {
		stopNow = true;
	}

	public static void centerFrame(int frameWidth, int frameHeight, Component c) {
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width - frameWidth) / 2;
		int yUpperLeftCorner = (screen.height - frameHeight) / 2;

		c.setBounds(xUpperLeftCorner, yUpperLeftCorner, frameWidth, frameHeight);
	}

	public static void main(String[] args) {
		SimulationFrame spd = new SimulationFrame();
		spd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		spd.setVisible(true);
	}

}
