package visualization;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

public class GameExplanationPanel extends JPanel implements Runnable {
	private static final long serialVersionUID = -758132794406845296L;

	int amt = 1, count = 0, mode = 0;
	boolean running = false;
	double prc = 0;

	String history = "";

	public GameExplanationPanel() {
		setBackground(Color.BLACK);
		Dimension size = new Dimension(500, 400);
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setSize(size);
	}

	public void paintComponent(Graphics gO) {
		super.paintComponent(gO);

		Graphics2D g = (Graphics2D) gO;

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.setColor(Color.WHITE);
		g.setStroke(new BasicStroke(2));

		g.setFont(new Font("SANS_SERIF", Font.PLAIN, 20));

		if (mode == 0) {
			g.drawArc(2 * getWidth() / 3 - 60, 4 * getHeight() / 11 - 60, 120, 120, (int) (prc * 360) - 170, (int) (60));

			int x = (int) (60 * Math.sin((prc + 1 / 6 - 11 / 36) * 2 * Math.PI)), y = (int) (60 * Math
					.cos((prc + 1 / 6 - 11 / 36) * 2 * Math.PI));

			int x1 = 2 * getWidth() / 3 + x, y1 = 4 * getHeight() / 11 + y;
			int x2 = 2 * getWidth() / 3 + x - y / 3 + x / 4, y2 = 4 * getHeight() / 11 + y + x / 3 - y / 4;
			int x3 = 2 * getWidth() / 3 + x - y / 3 - x / 4, y3 = 4 * getHeight() / 11 + y + x / 3 + y / 4;
			g.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);

			g.setColor(Visualization.colorMeld(Color.GREEN, Color.BLACK, 2 * Math.abs(prc - 0.5)));
			Visualization.drawCenteredText(g, "Heads!", 2 * getWidth() / 3, 4 * getHeight() / 11 - 80, getWidth() / 3,
					20);
		} else if (mode == 1) {
			int x = 2 * getWidth() / 3 + (int) (prc * getWidth() / 6), y = 7 * getHeight() / 11
					+ (int) (prc * getWidth() / 6);
			g.drawLine(x - 40, y - 40, x - 10, y - 10);
			int k = (int) (8 * Math.cos(prc * 2 * Math.PI));
			g.fillPolygon(new int[] { x, x - 12 - k, x - 12 + k }, new int[] { y, y - 12 + k, y - 12 - k }, 3);

			if (prc < .9) {
				g.setColor(Visualization.colorMeld(new Color(200, 200, 255), Color.BLACK, 2 * Math.abs(prc - 0.5)));
				Visualization.drawCenteredText(g, "Tails", 2 * getWidth() / 3, 7 * getHeight() / 11, getWidth() / 6,
						3 * getHeight() / 11);
			}

			if (1 - prc <= .02) {
				g.setColor(Color.GREEN);
				g.drawOval(x - 3, y - 3, 25 + g.getFontMetrics().stringWidth("+" + amt), 50);
				g.drawString("+" + amt, x + 10, y + 30);
			}
		}

		//g.setColor(new Color(40, 40, 100));
		g.setColor(Color.BLACK);
		g.fillRoundRect(getWidth() / 3, 4 * getHeight() / 11, getWidth() / 3, 3 * getHeight() / 11, 20, 20);
		g.setColor(new Color(180, 180, 240));
		g.drawRoundRect(getWidth() / 3, 4 * getHeight() / 11, getWidth() / 3, 3 * getHeight() / 11, 20, 20);
		g.setColor(Color.WHITE);
		g.setFont(new Font("SANS_SERIF", Font.BOLD, 30));
		Visualization.drawCenteredText(g, "$" + amt, getWidth() / 3, getHeight() / 2 - 30, getWidth() / 3, 30);
		Visualization.drawCenteredText(g, count + " flips", getWidth() / 3, getHeight() / 2, getWidth() / 3, 30);

		/*
		 * g.setColor(Color.WHITE);
		 * String[] split = history.split("\n");
		 * g.setFont(new Font("SANS_SERIF", Font.PLAIN, 12));
		 * 
		 * for (int i = 0; i < split.length; i++) {
		 * g.drawString(split[i], 10, 90 + (g.getFontMetrics().getHeight() + 4)
		 * * i);
		 * }
		 */
	}

	public void play() {
		if (!running) {
			prc = 0;
			mode = 0;
			count = 0;
			amt = 1;
			new Thread(this).start();
		}
	}

	public void run() {
		if (!running) {
			running = true;
			amt = 1;
			boolean coin = (int) (Math.random() * 2) == 0;
			history = "";
			repaint();

			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}

			while (coin) {
				prc = 0;
				for (int i = 0; i < 30; i++) {
					try {
						Thread.sleep(30);
					} catch (Exception e) {
					}

					if (i == 10) {
						amt *= 2;
						coin = (int) (Math.random() * 2) == 0;
						count++;
						history += "Flipped heads\n";
						repaint();
					}
					prc = i / 30D;
					repaint();
				}
				prc = 0;
				repaint();
			}

			mode = 1;
			for (int i = 0; i < 20; i++) {
				try {
					Thread.sleep(30);
				} catch (Exception e) {
				}

				if (i == 13) {
					history += "Flipped tails";
					count++;
				}
				prc = i / 20D;
				repaint();
			}
			prc = 1;
			repaint();
		}
		running = false;
	}
}
