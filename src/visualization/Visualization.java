package visualization;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Visualization extends JPanel implements Runnable, ActionListener, ChangeListener, KeyListener {
	private static final long serialVersionUID = -9079048434684247226L;

	public Font font = new Font("SANS_SERIF", Font.PLAIN, 18);

	int bigDelay = 100; // in between rounds

	public static Color BACK = Color.BLACK, FORE = Color.WHITE;

	long funds = 0;
	long biggestDebt = 0;
	long numTries = 0;
	long mostEarned = 0;
	long total = 0;
	int cost = 8;
	boolean stopNow = true;

	int recordSize = 30;
	double[] profits = new double[recordSize];
	double[] moneyRecord = new double[recordSize];
	int[] profitsTo = new int[recordSize];
	int[] moneyRecordTo = new int[recordSize];
	double xReload = 0;
	int minPTo, maxPTo, maxMTo, minMTo;
	double minP, maxP, minM, maxM;

	JPanel controls = new JPanel(new FlowLayout(FlowLayout.CENTER));
	// not added to panel, but to frame on south
	JButton start = new JButton("Start");
	JButton stop = new JButton("Stop");
	JSlider speed = new JSlider(JSlider.HORIZONTAL, 1, 3000, 100);
	JSlider records = new JSlider(JSlider.HORIZONTAL, 4, 200, 30);
	JTextField costBox = new JTextField(3);

	public Visualization() {
		setDoubleBuffered(true);
		setBackground(BACK);
		start.setBackground(new Color(0, 30, 20));
		start.setFont(font);
		start.setFocusPainted(false);
		start.setForeground(FORE);
		start.addActionListener(this);
		// start.setBorder(new LineBorder(Color.DARK_GRAY));

		stop.setBackground(BACK);
		stop.setEnabled(false);
		stop.setFont(font);
		stop.setFocusPainted(false);
		stop.setForeground(FORE);
		stop.addActionListener(this);

		costBox.setBackground(BACK);
		costBox.setHorizontalAlignment(JLabel.CENTER);
		costBox.setBorder(new LineBorder(Color.DARK_GRAY));
		costBox.setForeground(Color.GRAY);
		costBox.setText("8");
		costBox.setFont(font);
		costBox.addActionListener(this);

		speed.setOpaque(false);
		records.setOpaque(false);
		speed.addChangeListener(this);
		records.addChangeListener(this);

		controls.add(start);
		controls.add(Box.createHorizontalStrut(30));
		controls.add(createLabel("Price:"));
		controls.add(costBox);
		controls.add(Box.createHorizontalStrut(30));
		controls.add(createLabel("Speed:"));
		controls.add(speed);
		controls.add(createLabel("History Length:"));
		controls.add(records);
		controls.add(Box.createHorizontalStrut(30));
		controls.add(stop);
		controls.setBackground(BACK);

		new Thread(new SmootherThread()).start();
	}

	public void keyPressed(KeyEvent evt) {}
	public void keyReleased(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			if (isRunning())
				stop();
			else
				start();
		} else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
			controls.setVisible(!controls.isVisible());
		}
	}
	public void keyTyped(KeyEvent evt) {}

	private JLabel createLabel(String string) {
		JLabel lbl = new JLabel(string);
		lbl.setFont(font);
		lbl.setForeground(Color.LIGHT_GRAY);
		return lbl;
	}

	public void rollAndAdd(int profit, long money) {
		for (int i = 0; i < recordSize - 1; i++) {
			profits[i] = profits[i + 1];
			profitsTo[i] = profitsTo[i + 1];
			moneyRecord[i] = moneyRecord[i + 1];
			moneyRecordTo[i] = moneyRecordTo[i + 1];
		}
		profits[recordSize - 1] = 0;
		profitsTo[recordSize - 1] = profit;
		moneyRecord[recordSize - 1] = moneyRecord[recordSize - 2];
		moneyRecordTo[recordSize - 1] = (int) money;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setFont(font);
		g2d.setColor(FORE);

		// draw profits
		if (!stopNow) {
			minPTo = Math.min(min(profitsTo), 0);
			maxPTo = Math.max(max(profitsTo), 0);
			minMTo = Math.min(min(moneyRecordTo), 0);
			maxMTo = Math.max(max(moneyRecordTo), 0);
		} else {
			minPTo = -1;
			maxPTo = 1;
			minMTo = -1;
			maxMTo = 1;
		}

		g2d.clipRect(20, getHeight() / 4, getWidth() / 2 - 50, getHeight() / 2);
		if (maxP - minP > 0) {
			int h0 = (int) (getHeight() * (maxP / (maxP - minP))) / 2 + getHeight() / 4;
			double dw = (getWidth() - 50) / (2D * recordSize);
			g2d.setStroke(new BasicStroke(3));
			g2d.drawLine(20, h0, getWidth() / 2, h0);

			for (int i = 0; i < recordSize; i++) {
				// paint profits
				g2d.setColor(profits[i] > 0 ? new Color(150, 250, 150) : new Color(250, 150, 150));

				if (profits[i] > 0) {
					double prc = (maxP - (double) profits[i]) / (maxP - minP);
					int a = (int) ((i - xReload) * dw) + 20, b = getHeight() / 4 + (int) (getHeight() * prc / 2), c = (int) Math
							.max(dw - 3, 3), d = h0 - (getHeight() / 4 + (int) (getHeight() * prc / 2));
					g2d.fillRect(a, b, c, d);
					if (bigDelay >= 50 && recordSize <= 40 && profitsTo[i] != 0) {
						g.setColor(colorMeld(g.getColor(), BACK, .8));
						drawCenteredText(g2d, "" + Math.round(Math.log(profits[i] + cost) / Math.log(2)), a, b, c, d);
					}
				} else {
					int a = (int) ((i - xReload) * dw) + 20, c = (int) Math.max(dw - 3, 3), d = (int) ((getHeight() / 2 * (-profits[i])) / (maxP - minP));
					g2d.fillRect(a, h0, c, d);
					if (bigDelay >= 50 && recordSize <= 40 && profitsTo[i] != 0) {
						g.setColor(colorMeld(g.getColor(), BACK, .8));
						drawCenteredText(g2d, "" + Math.round(Math.log(profits[i] + cost) / Math.log(2)), a, h0, c, d);
					}
				}
			}
		}

		g2d.setClip(null);
		g2d.clipRect(getWidth() / 2 + 20, getHeight() / 4, getWidth() / 2 - 50, getHeight() / 2);
		if (maxM - minM > 0) {
			g.setColor(FORE);

			int h0 = (int) (getHeight() * (maxM / (maxM - minM))) / 2 + getHeight() / 4;
			double dw = (getWidth() - 50) / (2D * recordSize);
			g2d.drawLine(20 + getWidth() / 2, h0, getWidth() - 20, h0);
			g2d.setStroke(new BasicStroke(5));
			g.setColor(new Color(150, 150, 250));

			for (int i = 0; i < recordSize - 1; i++) {
				g2d.drawLine((int) ((i - xReload) * dw) + getWidth() / 2 + 20, moneyHeight(moneyRecord[i]), (int) ((i
						- xReload + 1) * dw)
						+ getWidth() / 2 + 20, moneyHeight(moneyRecord[i + 1]));
			}
		}
		g2d.setClip(null);
		g2d.setStroke(new BasicStroke(1));

		// draw statistics
		g2d.setColor(FORE);
		g2d.setFont(font.deriveFont(Font.PLAIN, 80));
		drawCenteredText(g2d, "$" + funds, getWidth() / 2, getHeight() / 4, getWidth() / 2, getHeight() / 2);
		drawCenteredText(g2d, "$" + (numTries == 0 ? 0 : NumberFormat.getNumberInstance().format(total / numTries)), 0,
				getHeight() / 4, getWidth() / 2, getHeight() / 2);

		g2d.setColor(FORE);
		g2d.setFont(font.deriveFont(Font.PLAIN, 50));
		drawCenteredText(g2d, "Game #" + numTries, 0, 0, getWidth(), getHeight() / 6);

		g2d.setFont(font.deriveFont(Font.PLAIN, 20));

		String spaces = "        ";
		drawCenteredText(
				g2d,
				"Cost: " + cost + spaces + " Luckiest Game: " + mostEarned + " ["
						+ Math.max(Math.round(Math.log(mostEarned) / Math.log(2)), 0) + " Flips]" + spaces
						+ "Max Debt: " + biggestDebt, 0, 3 * getHeight() / 4, getWidth(), getHeight() / 4);

	}

	private int moneyHeight(double d) {
		return getHeight() / 4 + (int) (getHeight() * (maxM - d) / (maxM - minM) / 2);
	}

	@Override
	public void stateChanged(ChangeEvent evt) {
		if (evt.getSource() == records) {
			double[] p = new double[records.getValue()];
			double[] m = new double[records.getValue()];
			int[] pt = new int[records.getValue()];
			int[] mt = new int[records.getValue()];

			for (int i = 1; i <= Math.min(records.getValue(), recordSize); i++) {
				p[records.getValue() - i] = profits[recordSize - i];
				m[records.getValue() - i] = moneyRecord[recordSize - i];
				pt[records.getValue() - i] = profitsTo[recordSize - i];
				mt[records.getValue() - i] = moneyRecordTo[recordSize - i];
			}

			recordSize = records.getValue();
			profits = p;
			moneyRecord = m;
			moneyRecordTo = mt;
			profitsTo = pt;
		} else if (evt.getSource() == speed) {
			bigDelay = 5000 / speed.getValue();
			System.out.println(bigDelay);
		}
	}

	public void start() {
		cost = Integer.parseInt(costBox.getText());
		stopNow = false;
		new Thread(this).start();
		start.setEnabled(false);
		stop.setEnabled(true);
		stop.setBackground(new Color(0, 30, 20));
		start.setBackground(BACK);
	}

	public void stop() {
		stopNow = true;
		funds = 0;
		biggestDebt = 0;
		numTries = 0;
		mostEarned = 0;
		total = 0;
		Arrays.fill(profitsTo, 0);
		Arrays.fill(moneyRecordTo, 0);
		stop.setEnabled(false);
		start.setEnabled(true);
		start.setBackground(new Color(0, 30, 20));
		stop.setBackground(BACK);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == start) {
			start();
		} else if (evt.getSource() == stop) {
			stop();
		} else if (evt.getSource() == costBox) {
			cost = Integer.parseInt(costBox.getText());
			this.requestFocus();
		}
	}

	private class SmootherThread implements Runnable {
		double K = Math.min((bigDelay * recordSize / 250D) + 1, 5);

		public void run() {
			while (true) {
				for (int i = 0; i < recordSize; i++) {
					profits[i] += (profitsTo[i] - profits[i]) / K;
					moneyRecord[i] += (moneyRecordTo[i] - moneyRecord[i]) / K;
				}

				minP += (minPTo - minP) / K;
				maxP += (maxPTo - maxP) / K;
				minM += (minMTo - minM) / K;
				maxM += (maxMTo - maxM) / K;

				if (stopNow)
					xReload *= .9;
				repaint();

				try {
					Thread.sleep(30);
				} catch (Exception e) {}
			}
		}
	}

	public void addKeyListenersForChildren(KeyListener k) {
		stop.addKeyListener(k);
		start.addKeyListener(k);
		records.addKeyListener(k);
		speed.addKeyListener(k);
	}

	public boolean isRunning() {
		return !stopNow;
	}

	public void run() {
		mainLoop: while (!stopNow) {
			funds -= cost;// player pays.

			boolean coin = (int) (Math.random() * 2) == 0;
			int tableMoneys = 1;

			while (coin) {
				tableMoneys *= 2;
				coin = (int) (Math.random() * 2) == 0;
			}

			funds += tableMoneys;
			total += tableMoneys;

			rollAndAdd((int) (tableMoneys - cost), funds);

			if (biggestDebt > funds)
				biggestDebt = funds;
			if (mostEarned < tableMoneys)
				mostEarned = tableMoneys;

			numTries++;

			for (int i = 0; i < Math.min(15, bigDelay); i++) {
				xReload = i / 15D;
				if (stopNow)
					break mainLoop;
				try {
					Thread.sleep(Math.max(1, bigDelay / 15));
				} catch (Exception e) {}
			}
			xReload = 0;
		}
	}

	public static void drawCenteredText(Graphics g, String s, int x, int y, int w, int h) {
		// Find the size of string s in font f in the current Graphics context
		FontMetrics fm = g.getFontMetrics(g.getFont());
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(s, g);

		int textHeight = (int) (rect.getHeight());
		int textWidth = (int) (rect.getWidth());

		// Center text horizontally and vertically
		int p = (w - textWidth) / 2 + x;
		int q = (h - textHeight) / 2 + fm.getAscent() + y;

		g.drawString(s, p, q); // Draw the string.
	}

	public static int min(int[] array) {
		int min = array[0];
		for (int i = 0; i < array.length; i++)
			if (array[i] < min)
				min = array[i];
		return min;
	}

	public static int max(int[] array) {
		int max = array[0];
		for (int i = 0; i < array.length; i++)
			if (array[i] > max)
				max = array[i];
		return max;
	}

	public static Color colorMeld(Color a, Color b, double ratio) {
		return new Color((int) (a.getRed() + (b.getRed() - a.getRed()) * ratio),
				(int) (a.getGreen() + (b.getGreen() - a.getGreen()) * ratio),
				(int) (a.getBlue() + (b.getBlue() - a.getBlue()) * ratio));
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("St. Petersburg Visual");
		// Visualization v = new Visualization();
		Visualization sim = new Visualization();
		frame.add(sim, BorderLayout.CENTER);
		frame.add(sim.controls, BorderLayout.NORTH);
		sim.controls.setVisible(false);
		sim.addKeyListenersForChildren(sim);
		frame.addKeyListener(sim);
		sim.addKeyListener(sim);
		// frame.add(v.controls, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setVisible(true);
	}
}
