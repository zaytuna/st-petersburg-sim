package visualization;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PresentorPanel extends JPanel implements KeyListener {
	private static final long serialVersionUID = 999608020028948450L;
	public static Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	public static final int NUM_SLIDES = 17, SIM_INDEX = 9, EXP_INDEX = 6;
	public static ImageIcon[] IMAGES = new ImageIcon[NUM_SLIDES];

	JLabel display = new JLabel();
	Visualization sim = new Visualization();
	GameExplanationPanel gep = new GameExplanationPanel();
	JPanel gepPanel = new JPanel(new GridBagLayout());

	int number = 0;
	static {
		for (int i = 0; i < NUM_SLIDES; i++) {
			Image img = new ImageIcon("Resources\\PAGE_" + i + ".png").getImage();
			IMAGES[i] = new ImageIcon(img.getScaledInstance(img.getWidth(null) * SCREEN.height / img.getHeight(null),
					SCREEN.height, Image.SCALE_SMOOTH));
		}
	}

	public PresentorPanel() {
		setLayout(new BorderLayout());
		setBackground(Color.BLACK);
		add(display, BorderLayout.CENTER);
		addKeyListener(this);
		display.addKeyListener(this);
		sim.addKeyListener(this);
		sim.addKeyListenersForChildren(this);
		sim.controls.setVisible(false);
		gepPanel.setOpaque(false);
		gepPanel.add(gep);

		display.setIcon(IMAGES[0]);
		display.setHorizontalAlignment(JLabel.CENTER);
	}

	@Override
	public void keyReleased(KeyEvent evt) {

		if (evt.getKeyCode() == KeyEvent.VK_SPACE || evt.getKeyCode() == KeyEvent.VK_RIGHT)
			update((number + 1) % NUM_SLIDES);
		else if (evt.getKeyCode() == KeyEvent.VK_LEFT)
			update((number + NUM_SLIDES - 1) % NUM_SLIDES);
		else if (evt.getKeyCode() >= 48 && evt.getKeyCode() <= 57)
			update(evt.getKeyCode() - 48);
		else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			if (number == SIM_INDEX) {
				if (sim.isRunning())
					sim.stop();
				else
					sim.start();
			} else if (number == EXP_INDEX) {
				gep.play();
			}
		} else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
			this.requestFocus();
			sim.controls.setVisible(!sim.controls.isVisible());
		}

	}

	public void update(int newNum) {
		newNum = newNum % NUM_SLIDES;
		if (newNum == SIM_INDEX) {
			removeAll();
			add(sim, BorderLayout.CENTER);
			add(sim.controls, BorderLayout.NORTH);
			invalidate();
			revalidate();
			repaint();
		} else if (newNum == EXP_INDEX) {
			removeAll();
			add(gepPanel, BorderLayout.CENTER);
			invalidate();
			revalidate();
			repaint();
		} else {
			display.setIcon(IMAGES[newNum]);
			removeAll();
			add(display, BorderLayout.CENTER);
			invalidate();
			revalidate();
			repaint();
		}
		number = newNum;
	}

	public void keyPressed(KeyEvent evt) {
	}

	public void keyTyped(KeyEvent evt) {
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("St. Petersburg Visual");
		// Visualization v = new Visualization();
		PresentorPanel p = new PresentorPanel();
		frame.add(p, BorderLayout.CENTER);
		frame.addKeyListener(p);
		// frame.add(v.controls, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.setSize(SCREEN);
		frame.setVisible(true);
	}
}
