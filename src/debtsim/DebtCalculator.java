package debtsim;

import java.util.Random;

public class DebtCalculator implements Runnable {
	public static int nRuns = 1000;
	public static Random rng = new Random();

	int fee;

	public DebtCalculator(int i) {
		fee = i;
	}

	public static void main(String[] args) {
		for (int f = 0; f < 30; f++) {
			new DebtCalculator(f).run();
			// new DebtCalculator(f).start();
		}
	}

	public void run() {
		double avgDebt = 0;
		int avgTries = 0;

		for (int i = 0; i < nRuns; i++) {
			long maxDebt = 0;
			long money = 0;
			while (money <= 0) {
				avgTries++;
				money -= fee;
				int table = 1;
				while (rng.nextInt(2) == 0)
					table *= 2;

				if (money < maxDebt)
					maxDebt = money;

				money += table;
			}
			avgDebt += maxDebt;
		}
		avgDebt /= nRuns;
		avgTries /= nRuns;
		System.out.println(fee + "\t" + Math.abs(avgDebt) + "\t" + avgTries);
		// return avgDebt;
	}
}